package com.daniel.proyect.gui;

/**
 * Created by daniel on 09/12/2016.
 */
public class Aplicacion {
    public static void main(String [] args){
        VentanaModel model = new VentanaModel();
        Ventana view = new Ventana();
        VentanaController controller = new VentanaController(view, model);
    }
}
