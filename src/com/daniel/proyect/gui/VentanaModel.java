package com.daniel.proyect.gui;

import com.daniel.proyect.util.Constantes;

import java.io.*;
import java.util.Vector;

/**
 * Created by daniel on 09/12/2016.
 */
public class VentanaModel {
    Vector<String>vHistorial=new Vector<String>();
    public VentanaModel(){
        File filPrograma = new File(Constantes.RUTA_PROGRAMA);
        File filDescargas = new File(Constantes.RUTA_DESCARGAS);
        File filHistorial = new File(Constantes.RUTA_HISTORIAL);
        filPrograma.mkdir();
        filDescargas.mkdir();
        filHistorial.mkdir();
    }
    public void setHistorial(){
        File fil = null;
        FileWriter fw = null;
        BufferedWriter bw = null;


        try {
            fil = new File(Constantes.RUTA_HISTORIAL + "/historial.dat");
            fw = new FileWriter(fil);
            bw = new BufferedWriter(fw);
            for (int i = 0; i < vHistorial.size(); i++) {
                bw.write(vHistorial.get(i));
                bw.newLine();
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public void addHistorial(String historial){
        vHistorial.add(historial);
    }
    public void crearHistorial(){
        File fil = null;
        FileReader fr = null;
        BufferedReader br = null;
        String texto = "";


        try {
            fil = new File(Constantes.RUTA_HISTORIAL + "/historial.dat");
            fr = new FileReader(fil);
            br = new BufferedReader(fr);
            String linea = "";
            while(linea != null){
                linea = br.readLine();
                texto += linea + "\n";
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        PnlHistorial pnl = new PnlHistorial();
        pnl.tbConsola.setToolTipText(texto);
        pnl.tbConsola.setText(pnl.tbConsola.getToolTipText());
    }
}
