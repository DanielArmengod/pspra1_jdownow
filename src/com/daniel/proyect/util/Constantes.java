package com.daniel.proyect.util;

/**
 * Created by daniel on 10/12/2016.
 */
public class Constantes {
    public final static String RUTA_PROGRAMA = System.getProperty("user.home") + "/JDowNow";
    public static String RUTA_DESCARGAS = RUTA_PROGRAMA + "/Descargas";
    public final static String RUTA_HISTORIAL = RUTA_PROGRAMA + "/Historial";
}
