package com.daniel.proyect.gui;


import com.daniel.proyect.util.Constantes;
import com.daniel.proyect.util.MotorDescarga;
import com.daniel.proyect.util.Util;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.Vector;

/**
 * Created by daniel on 09/12/2016.
 */
public class VentanaController implements ActionListener {
    private Ventana view;
    private VentanaModel model;

    private enum Modo{
        NUEVA, CANCELAR, REANUDAR
    }

    Vector<PnlDescarga>vPanelDescarga=new Vector<PnlDescarga>();
    Vector<MotorDescarga>vMotorDescarga = new Vector<MotorDescarga>();

    public VentanaController(Ventana view, VentanaModel model) {
        this.view = view;
        this.model = model;
        anadirActionsListeners();
    }

    private void anadirActionsListeners() {
        view.btAnadir.addActionListener(this);
        view.itemConsola.addActionListener(this);
        view.itemDestino.addActionListener(this);
    }

    private void anadirActionsListenersDescargas(){
        view.pnlDescarga.btIniciar.addActionListener(this);
        view.pnlDescarga.btEliminar.addActionListener(this);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "Elegir ruta destino":
                JFileChooser jf = new JFileChooser();
                int res = jf.showSaveDialog(null);
                if(res == 0){
                    File fil = new File(jf.getSelectedFile().getAbsoluteFile().getAbsolutePath());
                    fil.mkdir();
                    Constantes.RUTA_DESCARGAS = fil.getAbsolutePath();
                    model.addHistorial("Ruta descargas detino cambiada: " + fil.getAbsolutePath());
                }

                break;
            case "Ver historial por consola":
                model.crearHistorial();
                break;
            case "Anadir":
                if(comprobarCampo()){
                    nuevaDescarga();
                }
                break;
        }
        for (int i = 0; i < vPanelDescarga.size(); i++) {
            if(e.getSource() == vPanelDescarga.get(i).btIniciar) {
                switch (vPanelDescarga.get(i).btIniciar.getActionCommand()) {
                    case "Iniciar":
                        vPanelDescarga.get(i).btIniciar.setText("Cancelar");
                        descargarArchivos(i, Modo.NUEVA);
                        refrescarPanel();
                        model.addHistorial("Descarga inicializada: " + vPanelDescarga.get(i).lbUrl.getText());
                        break;
                    case "Reanudar":
                        vPanelDescarga.get(i).btIniciar.setText("Cancelar");
                        descargarArchivos(i, Modo.REANUDAR);
                        refrescarPanel();
                        model.addHistorial("Descarga reanudada: " + vPanelDescarga.get(i).lbUrl.getText());
                        break;
                    case "Cancelar":
                        vPanelDescarga.get(i).btIniciar.setText("Reanudar");
                        descargarArchivos(i, Modo.CANCELAR);
                        vPanelDescarga.get(i).pbProgreso.setValue(0);
                        model.addHistorial("Descarga cancelada: " + vPanelDescarga.get(i).lbUrl.getText());
                        break;
                }
            }
            if(e.getSource() == vPanelDescarga.get(i).btEliminar){
                eliminarPanel(i);
            }
        }
        model.setHistorial();
    }

    private void eliminarPanel(int i) {
        System.out.println(i);
        vPanelDescarga.remove(i);
        try {
            vMotorDescarga.get(i).cancel(true);
            vMotorDescarga.remove(i);
            model.addHistorial("Descarga: eliminada.");
        }catch(Exception ex){
            System.out.println("Descarga no inicializada eliminada.");
        }
        anadirPaneles();
        refrescarPanel();
    }

    private boolean comprobarCampo() {
        if(view.tbUrl.getText().equals("")){
            Util.mensajeAlerta("Inserte una URL de una descarga directa.", "Error al añadir descarga");
            return false;
        }else{
            return true;
        }
    }

    private void nuevaDescarga() {
        view.pnlDescarga = new PnlDescarga();
        anadirActionsListenersDescargas();
        view.pnlDescarga.lbUrl.setText(view.tbUrl.getText());
        vPanelDescarga.add(view.pnlDescarga);
        anadirPaneles();
        refrescarPanel();
        model.addHistorial("Descarga creada: " + view.tbUrl.getText());
    }

    private void anadirPaneles() {
        view.panelDescargas.removeAll();
        for(PnlDescarga pnlDescarga : vPanelDescarga){
            view.panelDescargas.add(pnlDescarga.panelDescarga);
        }
    }

    private void refrescarPanel() {
        view.panelDescargas.revalidate();
        view.panelDescargas.repaint();
    }

    public void descargarArchivos(int i, Modo mode){
        switch (mode){
            case CANCELAR:
                System.out.println("Cancelado num: " + i);
                System.out.println("Numero de descargas: " + vMotorDescarga.size());
                vMotorDescarga.get(vPanelDescarga.get(i).idDescarga).canelarDescarga(true);
                break;
            case NUEVA:
            case REANUDAR:
                MotorDescarga descarga = null;
                File rutaFichero = new File(Constantes.RUTA_DESCARGAS);
                try {
                    String url = vPanelDescarga.get(i).lbUrl.getText();
                    descarga = new MotorDescarga(url, rutaFichero.getAbsolutePath() + url.substring(url.lastIndexOf("/"), url.length()));
                    if (mode == Modo.NUEVA) {
                        vMotorDescarga.add(descarga);
                        vPanelDescarga.get(i).idDescarga = vMotorDescarga.size() - 1;
                    }else{
                        vMotorDescarga.setElementAt(descarga, vPanelDescarga.get(i).idDescarga);
                    }
                    descarga.addPropertyChangeListener(new PropertyChangeListener() {
                        @Override
                        public void propertyChange(PropertyChangeEvent evt) {
                            switch (evt.getPropertyName()) {
                                case "progress":
                                    vPanelDescarga.get(i).pbProgreso.setValue((Integer) evt.getNewValue());
                                    vPanelDescarga.get(i).lbProgreso.setText(Util.tamano(vMotorDescarga.get(vPanelDescarga.get(i).idDescarga).getProgresoDescarga(), vMotorDescarga.get(vPanelDescarga.get(i).idDescarga).getTamanoFichero()));
                                    System.out.println(vMotorDescarga.get(vPanelDescarga.get(i).idDescarga).getProgresoDescarga());
                                    break;
                                case "finalizada":
                                    model.addHistorial("Descarga finalizada: " + vPanelDescarga.get(i).lbUrl.getText());
                                    break;
                            }
                        }

                    });
                    descarga.execute();
                } catch (Exception e) {
                    if (e instanceof MalformedURLException)
                        Util.mensajeError("Error de url¡¡", "La url no es la correcta");
                    else if (e instanceof FileNotFoundException)
                        Util.mensajeError("Error de origen¡¡", "No se ha podido leer el fichero de origen");
                    else
                        Util.mensajeError("Error¡¡", "Compruebe el origen de fichero");
                    e.printStackTrace();
                }
                break;
        }
    }

}
