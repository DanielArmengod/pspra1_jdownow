package com.daniel.proyect.util;

import javax.swing.*;
import java.text.DecimalFormat;

/**
 * Created by daniel on 09/12/2016.
 */
public class Util {
    public static void mensajeAlerta(String title, String text) {
        JOptionPane.showMessageDialog(null, text, title, JOptionPane.WARNING_MESSAGE);
    }
    public static void mensajeError(String title, String text){
        JOptionPane.showMessageDialog(null, text, title, JOptionPane.ERROR_MESSAGE);
    }
    public static String tamano(long progreso, long tamano){
        double progresMb = progreso / 1024;
        double tamanoMb = tamano / 1024;
        DecimalFormat dec = new DecimalFormat("###,###.##");
        String progMb = dec.format(progresMb).concat(" MB");
        String tamanMb = dec.format(tamanoMb).concat(" MB");
        return progMb + " de, " + tamanMb;
    }
}
