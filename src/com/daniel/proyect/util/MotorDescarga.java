package com.daniel.proyect.util;

import javax.swing.*;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by daniel on 10/12/2016.
 */
public class MotorDescarga extends SwingWorker<Void, Integer> {

    private String urlFichero;
    private String rutaFichero;
    private int longitud = 0;
    private long progresoDescarga = 0;
    private boolean cancelada = false;
    private int tamanoFichero;

    public int getLongitud() {
        return longitud;
    }

    public String getUrlFichero() {
        return urlFichero;
    }

    public String getRutaFichero() {
        return rutaFichero;
    }

    public long getProgresoDescarga() {
        return progresoDescarga;
    }

    public int getTamanoFichero() {
        return tamanoFichero;
    }

    public MotorDescarga(String urlFichero, String rutaFichero){
        this.urlFichero = urlFichero;
        this.rutaFichero = rutaFichero;
    }
    @Override
    protected Void doInBackground() throws Exception {
        URL url = new URL(urlFichero);
        URLConnection conexion = url.openConnection();

        tamanoFichero = conexion.getContentLength();

        InputStream in = url.openStream();
        FileOutputStream out = new FileOutputStream(rutaFichero);
        byte[] bytes = new byte[tamanoFichero];

        while((longitud = in.read(bytes)) != -1 && !cancelada){
            if(cancelada)
                continue;
            out.write(bytes, 0, longitud);
            progresoDescarga += longitud;
            setProgress((int) (progresoDescarga * 100 / tamanoFichero));
        }
        in.close();
        out.close();
        if(!cancelada) {
            setProgress(100);
        }

        firePropertyChange("finalizada", 0, 1);
        return null;
    }

    public void canelarDescarga(boolean b) {
        this.cancelada = b;
        progresoDescarga = 0;
        longitud = 0;
    }
}
