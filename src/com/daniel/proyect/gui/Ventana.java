package com.daniel.proyect.gui;

import javax.swing.*;

/**
 * Created by daniel on 10/12/2016.
 */
public class Ventana {
    JPanel panelDescargas;
    JButton btAnadir;
    JTextField tbUrl;
    JPanel panel1;
    JMenuBar jMenuBar;
    JMenuItem itemDestino;
    JMenuItem itemConsola;
    JButton btDescargarT;
    JButton btCancelarT;
    JButton btEliminarT;
    JButton btDescargarTC;
    PnlDescarga pnlDescarga;

    public Ventana() {
        JFrame frame = new JFrame("Ventana");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(3);
        frame.pack();
        frame.setSize(frame.getWidth(), 500);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setVisible(true);
        anadirItemsMenu();
    }

    private void anadirItemsMenu() {
        JMenu menu = new JMenu("Opciones");
        itemDestino = new JMenuItem("Elegir ruta destino");
        itemConsola = new JMenuItem("Ver historial por consola");
        jMenuBar.add(menu);
        menu.add(itemDestino);
        menu.add(itemConsola);
    }

    private void createUIComponents() {
        panelDescargas = new JPanel();
        panelDescargas.setLayout(new BoxLayout(panelDescargas, BoxLayout.Y_AXIS));
    }
}
